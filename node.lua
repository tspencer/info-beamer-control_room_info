gl.setup(NATIVE_WIDTH, NATIVE_HEIGHT)

local json = require "json"
local image = resource.load_image "earth.png"
local logo = resource.load_image "EDRS_logo_pillars.jpg"


local font = resource.load_font("arial.ttf")
local st = util.screen_transform(90)

local eventfeed
local base_time = N.base_time or 0

util.data_mapper{
    ["clock/set"] = function(time)
        base_time = tonumber(time) - sys.now()
        N.base_time = base_time
    end;
}

function draw_info()
    util.draw_correct(image, WIDTH/2, HEIGHT/2, WIDTH*1.4, HEIGHT*1.4)
    local time = base_time + sys.now()

    local UTC_font_sieze = 120
    local LOCAL_font_sieze = 70

    font:write(5, 5, "Control Room: K1", UTC_font_sieze, 0,0,0,1)
    font:write(5, 5+UTC_font_sieze, os.date("%a %d %b %X", time), LOCAL_font_sieze, 0,0,0,1)


    util.draw_correct(logo, 5, 400, 5+LOCAL_font_sieze, 400+LOCAL_font_sieze)
    font:write(5+5+LOCAL_font_sieze, 400,"EDRS shedule", LOCAL_font_sieze, 0,0,0,1)

    local events_y = HEIGHT/3
    local events_height = HEIGHT/2

    local events_font_sieze = 60
    local events_to_display = math.floor((HEIGHT - events_height) / events_font_sieze)
    local events_margin_x = 50
    local events_margin_y = ((HEIGHT - events_height) - (events_to_display * events_font_sieze)) / (events_to_display + 1)


    for i,line in pairs(eventfeed) do
      if i > events_to_display then break end

      local start_time = makeTimeStamp(line['start_time'])
      local stop_time = makeTimeStamp(line['stop_time'])
      local time_to_event = time-start_time
      local start_time_width = 0
      -- print(time_to_event)
      if start_time > time then
        start_time_width = font:write(events_margin_x, events_y+(i-1)*events_font_sieze+events_margin_y*(i), os.date('%H:%M:%S', time_to_event), events_font_sieze, 0,0,0,1)
      elseif start_time < time then
        start_time_width = font:write(events_margin_x, events_y+(i-1)*events_font_sieze+events_margin_y*(i), os.date('%H:%M:%S', stop_time-time_to_event), events_font_sieze, 0,0,0,1)
      end
      -- local start_time_width = font:write(events_margin_x*10, events_y+(i-1)*events_font_sieze+events_margin_y*(i), , events_font_sieze, 0,0,0,1)
      -- local stop_time_width = font:write(events_margin_x*15, events_y+(i-1)*events_font_sieze+events_margin_y*(i), line['stop_time'], events_font_sieze, 0,0,0,1)

      local station_width = font:write(events_margin_x*2+start_time_width, events_y+(i-1)*events_font_sieze+events_margin_y*(i), line['activity'], events_font_sieze, 0,0,0,1)
    end
end

function makeTimeStamp(dateString)
    --     local pattern = "(%d+)%-(%d+)%-(%d+)T(%d+):(%d+):(%d+)([%+%-])(%d+)%:(%d+)"
    local pattern = "(%d+)%-(%d+)%-(%d+)T(%d+):(%d+):(%d+)"
    local xyear, xmonth, xday, xhour, xminute, xseconds = dateString:match(pattern)
    local convertedTimestamp = os.time({year = xyear, month = xmonth, day = xday, hour = xhour, min = xminute, sec = xseconds})
    -- local offset = xoffsethour * 60 + xoffsetmin
    -- if xoffset == "-" then offset = offset * -1 end
    -- return convertedTimestamp + offset
    return convertedTimestamp
end

function node.render()
    st()
    gl.clear(1,1,1,1)
    draw_info()
end

util.json_watch("eventfeed.json", function(raw)
    eventfeed = raw
    -- eventfeed = json.decode(raw)
    -- rPrint(eventfeed,nil,"test")
    -- settings contains the decoded json data as a Lua data structure
end)
